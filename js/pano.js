var map_;

var widget_;
var singleWidget_;
var shownMarker_;

var PROP_is_saved = 'PS_PROP_IS_SAVED';

var EVENT_click = 'click';

var ONE_SECOND = 1000;
var KEYCODE_LEFT = 37;
var KEYCODE_RIGHT = 39;
var KEY_ENTER = '13';

var beginBarrierCount = 2;

function maps_init() {
  if (--beginBarrierCount == 0)
    startApp();
}

$(document).ready(function() {
  $('#loading').css('visibility', 'hidden');
  setupLocationBox();
  setupSideBar();

  if (--beginBarrierCount == 0) {
    startApp();
  }
});

function setupSideBar() {
  $('#photo_back').click(function() {
    $('#slide').show();
    $('#single').hide();
  });
}

function addressToLatLngBounds(address, callback) {
  var request = {};
  request.address = address;

  var geocoder = new google.maps.Geocoder();
  geocoder.geocode(request, function(results, status) {
    if (status == 'OK' && results && results.length > 0) {
      var first = results[0];
      var bounds = first.geometry.bounds;
      callback(bounds);
    } else {
      callback(null);
    }
  });
}

function startApp() {
  var address = document.location.hash;
  if (address.length > 1) {
    address = lightUrlDecode(address.substr(1));
    $('#loc').val(address);

    addressToLatLngBounds(address, function(bounds) {
      startAppWithBounds(bounds);
    });
  } else {
    startAppWithBounds(null);
  }
}

function startAppWithBounds(opt_bounds) {
  if (!opt_bounds) {
    $('#loc').val('San Francisco, CA');
  }
  var bounds = opt_bounds || (new google.maps.LatLngBounds(
    new google.maps.LatLng(-50, -50),
    new google.maps.LatLng(50, 50)));

  setupPhotoWidget(bounds);
  setupWidgetKeyShortcuts();
  setupGoogleMap(bounds);
  setupMapBoundsChangeHandlers();
}

function setupWidgetKeyShortcuts() {
  $('body').keydown(function (e) {
    if ($('#loc').hasClass('focus')) {
      return;
    }

    if ($('#slide').css('display') == 'none') {
      $('#slide').show();
      $('#single').hide();
      $('#photo_info').hide();
    }

    if (e.keyCode == KEYCODE_LEFT) {
      var curpos = widget_.getPosition();
      if (widget_.getAtStart() == false) {
        setWidgetPosition(curpos - 1);
      }
    } else if (e.keyCode == KEYCODE_RIGHT) {
      var curpos = widget_.getPosition();
      if (widget_.getAtEnd() == false) {
        setWidgetPosition(curpos + 1);
      }
    }
  });

  $('#loc').focus(function() {
    $(this).addClass('focus');
  }).blur(function() {
    $(this).removeClass('focus');
  });
}

function lightUrlEncode(urlPart) {
  var newUrl = urlPart;
  newUrl = newUrl.replace(/ /g, '+');
  return newUrl;
}

function lightUrlDecode(urlEncoded) {
  var newUrl = urlEncoded;
  newUrl = newUrl.replace(/\+/g, ' ');
  return newUrl;
}

function setupLocationBox() {
  $('#loc').keypress(function (e) {
    if (e.which == KEY_ENTER) {
      var address = $('#loc').val();

      addressToLatLngBounds(address, function(bounds) {
        if (bounds) {
	  document.location.replace('#' + lightUrlEncode(address));
          map_.fitBounds(bounds);

          // widget
          var opt = {};
          opt.rect = mapBoundsToPanoramioRect(bounds);
          var req = new panoramio.PhotoRequest(opt);

          widget_.setRequest(req);
	  setWidgetPosition(0);
	}
      });
    }
  });
}

function setupPhotoWidget(initialMapBounds) {
  var initPhotoRequest = {rect: mapBoundsToPanoramioRect(initialMapBounds)};

  var options = {'width': 650, 'height': 600};
  widget_ = new panoramio.PhotoWidget('slide', initPhotoRequest, options);
  singleWidget_ = new panoramio.PhotoWidget('single', undefined, options);
  panoramio.events.listen(widget_, panoramio.events.EventType.PHOTO_CHANGED, onPhotoChanged);

  console.log(widget_.getAtEnd());
  console.log(widget_.getAtStart());

  setWidgetPosition(0);

  console.log(widget_.getAtEnd());
  console.log(widget_.getAtStart());
}


function setupGoogleMap(initialMapBounds) {
  var mapTypeControlOptions = {};
  mapTypeControlOptions.position = google.maps.ControlPosition.LEFT_BOTTOM;

  var zoomControlOptions = {};
  zoomControlOptions.style = google.maps.ZoomControlStyle.SMALL;
  zoomControlOptions.position = google.maps.ControlPosition.LEFT_TOP;

  var myOptions = {
    zoom: 8,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeControlOptions: mapTypeControlOptions,
    zoomControlOptions: zoomControlOptions,
    panControl: false,
    keyboardShortcuts: false
  }
  map_ = new google.maps.Map(document.getElementById('map_holder'), myOptions);
  map_.fitBounds(initialMapBounds);
}


function setupMapBoundsChangeHandlers() {
  var timeout;
  // Don't add this handler immediately, because maps API triggers zoom_changed
  // and center_changed right after the map is created, even if no user action
  // happened.
  setTimeout(function() {
    google.maps.event.addListener(map_, 'mouseout', function() {
      // Immediately call the timeout if it's set.
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
        onMapBoundsChange();
      }
    });
    google.maps.event.addListener(map_, 'zoom_changed', function() {
      // Skip the timeout, handle map change
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      onMapBoundsChange();
    });
    google.maps.event.addListener(map_, 'center_changed', function() {
      if (timeout) {
        clearTimeout(timeout);
      }
      timeout = setTimeout(function() {
        onMapBoundsChange();
      }, ONE_SECOND);
    });
  }, ONE_SECOND / 4);
}


function onMapBoundsChange() {
  console.log('onMapBoundsChange');

  var opt = {};
  opt.rect = mapBoundsToPanoramioRect(map_.getBounds());
  var req = new panoramio.PhotoRequest(opt);

  widget_.setRequest(req);
  setWidgetPosition(0);
}


function setWidgetPosition(newPosition) {
  $('#loading').css('visibility', 'visible');
  widget_.setPosition(newPosition);
}

function onPhotoChanged(event) {
  $('#loading').css('visibility', 'hidden');
  var photo = event.target.getPhoto();

  if (shownMarker_ && !shownMarker_[PROP_is_saved]) {
    shownMarker_.setMap(null);
  }

  var pos = photo.getPosition();
  if (pos) {
    var latLng = new google.maps.LatLng(pos.lat, pos.lng);
    shownMarker_ = new google.maps.Marker({
      position: latLng,
      map: map_
    });
  }
}


function mapBoundsToPanoramioRect(mapBounds) {
  var b = mapBounds;
  var rect = {};
  rect.sw = {};
  rect.ne = {};
  rect.sw.lat = b.getSouthWest().lat();
  rect.sw.lng = b.getSouthWest().lng();
  rect.ne.lat = b.getNorthEast().lat();
  rect.ne.lng = b.getNorthEast().lng();
  return rect;
}
