from google.appengine.ext import webapp
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app
import os
import logging
from urlparse import urlparse


class MainPage(webapp.RequestHandler):
  def get(self):
    dict = {}
    path = os.path.join(os.path.dirname(__file__), 'pano.html')
    self.response.out.write(template.render(path, dict))


application = webapp.WSGIApplication(
  [('/', MainPage),
   ],
  debug=True)


def main():
  run_wsgi_app(application)

if __name__ == '__main__':
  main()
